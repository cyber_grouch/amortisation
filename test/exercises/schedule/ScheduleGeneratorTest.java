package exercises.schedule;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.hamcrest.number.BigDecimalCloseTo;
import org.junit.Test;

import exercises.model.ScheduleItem;
import exercises.model.ScheduleParameters;

public class ScheduleGeneratorTest {

	@Test
	public void testInstantiation() {
        ScheduleGenerator schedule = new ScheduleGenerator();
		assertNotNull(schedule);
	}
	
	@Test
	public void testGenerateScheduleItems() {
	    ScheduleGenerator schedule = new ScheduleGenerator();
	    ScheduleParameters parameter = new ScheduleParameters(240000.000, 12.5, 3);
	    Optional<List<ScheduleItem>> items = schedule.generateSchedule(parameter);
	    
	    assertNotNull(schedule);
	    assertTrue(items.isPresent());
	}
	
	   
    @Test
    public void testGeneratedScheduleItemsCount() {
        int years = 3;
        ScheduleGenerator schedule = new ScheduleGenerator();
        ScheduleParameters parameter = new ScheduleParameters(240000.000, 12.5, years);
        Optional<List<ScheduleItem>> items = schedule.generateSchedule(parameter);
        
        int months = years * 12;
        
        assertNotNull(schedule);
        assertTrue(items.isPresent());
        assertTrue(months + 1 == items.get().size() || months + 2 == items.get().size());
    }
    
    @Test
    public void testGeneratedScheduleItemsConsistent() {
        int years = 3;
        ScheduleGenerator schedule = new ScheduleGenerator();
        ScheduleParameters parameter = new ScheduleParameters(240000.000, 12.5, years);
        Optional<List<ScheduleItem>> itemsO = schedule.generateSchedule(parameter);
        assertTrue(itemsO.isPresent());
        
        List<ScheduleItem> items = itemsO.get();
        for(int i = 1; i < items.size(); i++) {
            ScheduleItem current = items.get(i);
            ScheduleItem previous = items.get(i - 1);
            
            BigDecimal calculatedBalance = previous.getCurBalance()
                                                   .subtract(current.getCurMonthlyPaymentAmount())
                                                   .add(current.getCurMonthlyInterest());
            
            assertThat(String.format("not consistent for items %s (%s) and %s (%s)",
                                     i, calculatedBalance,
                                     (i - 1), current.getCurBalance()),
                       current.getCurBalance(),
                       BigDecimalCloseTo.closeTo(calculatedBalance, ERROR));
        }
                
        int months = years * 12;
        
        assertNotNull(schedule);
        assertTrue(months + 1 == items.size() || months + 2 == items.size());
    }
    
    @Test
    public void testLastItemCurBalanceIsZero() {
        int years = 5;
        ScheduleGenerator schedule = new ScheduleGenerator();
        ScheduleParameters parameter = new ScheduleParameters(240000.000, 12.5, years);
        List<ScheduleItem> items = schedule.generateSchedule(parameter).get();
        BigDecimal expected = BigDecimal.ZERO;
        BigDecimal actual = items.get(items.size() - 1).getCurBalance().setScale(2, BigDecimal.ROUND_DOWN);
        assertThat(String.format("not equal: expected (%s) and actual (%s)", expected, actual),
                   actual,
                   BigDecimalCloseTo.closeTo(expected, ERROR));
    }
    
    
    @Test
    public void testFirstItemCurBalanceAmountBorrowed() {
        int years = 5;
        ScheduleGenerator schedule = new ScheduleGenerator();
        ScheduleParameters parameter = new ScheduleParameters(240000.000, 12.5, years);
        List<ScheduleItem> items = schedule.generateSchedule(parameter).get();
        BigDecimal expected = new BigDecimal("240000.00");
        BigDecimal actual = items.get(0).getCurBalance();
        assertThat(String.format("not equal: expected (%s) and actual (%s)", expected, actual),
                   actual,
                   BigDecimalCloseTo.closeTo(expected, ERROR));
    }
    
    private static final BigDecimal ERROR = new BigDecimal(0.0009);

}
