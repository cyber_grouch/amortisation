package exercises.schedule;

import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.hamcrest.number.BigDecimalCloseTo;
import org.junit.Test;

public class CalculatorUtilsTest {

    @Test
    public void testScale() {
        BigDecimal actual = CalculatorUtils.scale(240000);
        BigDecimal expected = new BigDecimal(240000);
        assertThat(String.format("expected (%s), actual (%s)", expected, actual),
                   actual,
                   BigDecimalCloseTo.closeTo(expected, ERROR));
    }

    @Test
    public void testDivide() {
        BigDecimal actual = CalculatorUtils.divide(CalculatorUtils.scale(0.125),
                                                   CalculatorUtils.scale(12));
        BigDecimal expected = CalculatorUtils.divide(new BigDecimal("0.01041666666666666666666666666666666666666666"),
                                                     BigDecimal.ONE);
        assertThat(String.format("expected (%s), actual (%s)", expected, actual),
                   actual,
                   BigDecimalCloseTo.closeTo(expected, ERROR));
    }
    

    @Test
    public void testInverse() {
        BigDecimal actual = CalculatorUtils.inverse(CalculatorUtils.divide(CalculatorUtils.scale(0.125),
                                                                           CalculatorUtils.scale(12))
                                                                   .add(BigDecimal.ONE));
        BigDecimal expected = new BigDecimal("0.9896907216494845360824742268041237");
        assertThat(String.format("expected (%s), actual (%s)", expected, actual),
                   actual,
                   BigDecimalCloseTo.closeTo(expected, ERROR));
    }

    @Test
    public void testPower() {
        BigDecimal actual = CalculatorUtils.power(CalculatorUtils.inverse(CalculatorUtils.divide(CalculatorUtils.scale(0.125),
                                                                                                 CalculatorUtils.scale(12))
                                                                                         .add(BigDecimal.ONE)),
                                                  36);
        BigDecimal expected = new BigDecimal("0.6886236846652403444986337625066308");
        assertThat(String.format("expected (%s), actual (%s)", expected, actual),
                   actual,
                   BigDecimalCloseTo.closeTo(expected, ERROR));
    }
    
    public static final BigDecimal ERROR = new BigDecimal(0.0009);


}
