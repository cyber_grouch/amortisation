package exercises.schedule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import exercises.model.DerivedParameters;
import exercises.model.ScheduleItem;
import exercises.model.ScheduleParameters;

public class CalculatorTest {


    ScheduleParameters parameter;
    Calculator calculator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        parameter = new ScheduleParameters(240000.000, 12.5, 3);
        calculator = new Calculator(parameter);
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void testDerivedParameter() {
        DerivedParameters derivedParameters = calculator.getDerivedParameters();
        assertNotNull(derivedParameters);
    }

    @Test
    public void testDerivedParameter_BorrowedAmount() {
        DerivedParameters derivedParameters = calculator.getDerivedParameters();
        BigDecimal amountBorrowed = derivedParameters.getAmountBorrowed();
        assertTrue(new BigDecimal(240000).compareTo(amountBorrowed) == 0);
    }

    @Test
    public void testDerivedParameter_MonthlyPaymentAmount() {
        DerivedParameters derivedParameters = calculator.getDerivedParameters();
        BigDecimal actual = derivedParameters.getMonthlyPaymentAmount().setScale(2, BigDecimal.ROUND_DOWN);
        BigDecimal expected = new BigDecimal("8028.87");
        assertTrue(String.format("expected (%s), actual (%s)", expected, actual),
                   expected.compareTo(actual) == 0);
    }

    @Test
    public void testDerivedParameter_MonthlyInterest() {
        DerivedParameters derivedParameters = calculator.getDerivedParameters();
        BigDecimal actual = derivedParameters.getMonthlyInterest();
        BigDecimal expected = CalculatorUtils.divide(new BigDecimal("0.01041666666666666666666666666666666666666666"),
                                                     BigDecimal.ONE);
        assertTrue(String.format("expected (%s), actual (%s)", expected, actual),
                   expected.compareTo(actual) == 0);
    }
    
    @Test
    public void testGetFirstScheduleItem() {
        ScheduleItem firstScheduleItem = calculator.getFirstScheduleItem();
        
        assertNotNull(firstScheduleItem);
        assertEquals(0, firstScheduleItem.getPaymentNumber());
        assertTrue(BigDecimal.ZERO.compareTo(firstScheduleItem.getTotalInterestPaid()) == 0);
        assertTrue(BigDecimal.ZERO.compareTo(firstScheduleItem.getTotalPayments()) == 0);
        assertTrue(BigDecimal.ZERO.compareTo(firstScheduleItem.getCurMonthlyInterest()) == 0);
        assertTrue(BigDecimal.ZERO.compareTo(firstScheduleItem.getCurMonthlyPaymentAmount()) == 0);
        assertTrue(CalculatorUtils.scale(240000).compareTo(firstScheduleItem.getCurBalance()) == 0);
    }
    
    @Test
    public void testGetSecondScheduleItem() {
        ScheduleItem firstScheduleItem = calculator.getFirstScheduleItem();
        ScheduleItem secondScheduleItem = calculator.getNextScheduleItem(firstScheduleItem).get();
        
        assertNotNull(secondScheduleItem);

        assertEquals(1, secondScheduleItem.getPaymentNumber());
        
        BigDecimal expected = secondScheduleItem.getCurBalance()
                                                .subtract(firstScheduleItem.getCurBalance())
                                                .add(secondScheduleItem.getCurMonthlyPaymentAmount());
        BigDecimal actual = secondScheduleItem.getCurMonthlyInterest();

        assertTrue(String.format("expected (%s) actual (%s)",
                                 expected,
                                 actual),
                   BigDecimal.ZERO.compareTo(expected.subtract(actual)) == 0);
        
        BigDecimal expected1 = new BigDecimal("2500.00");
        BigDecimal actual1 = secondScheduleItem.getTotalInterestPaid().setScale(2, BigDecimal.ROUND_DOWN);
        assertTrue(String.format("expected (%s) actual (%s)",
                                 expected1,
                                 actual1),
                   BigDecimal.ZERO.compareTo(expected1.subtract(actual1)) == 0);
        
        BigDecimal expected2 = CalculatorUtils.scale(secondScheduleItem.getPaymentNumber())
                                              .multiply(secondScheduleItem.getCurMonthlyPaymentAmount());
        BigDecimal actual2 = secondScheduleItem.getTotalPayments();
        assertTrue(String.format("expected (%s), actual (%s)", expected2, actual2),
                   BigDecimal.ZERO.compareTo(expected2.subtract(actual2)) == 0);
    }


}
