package exercises.model;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ScheduleParametersTest {

    ScheduleParameters parameters;

    @Before
    public void setUp() throws Exception {
        parameters = new ScheduleParameters(240000.000, 12.5, 3);
    }

    @Test
    public void testAmount() {
        assertTrue(new Double(240000d).equals(parameters.getAmount()));
    }

    @Test
    public void testInterestRate() {
        assertTrue(new Double(12.5).equals(parameters.getInterestRate()));
    }

    @Test
    public void testYears() {
        assertTrue(new Integer(3).equals(parameters.getYears()));
    }
}
