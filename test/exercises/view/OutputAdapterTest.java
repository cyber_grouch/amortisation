package exercises.view;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.hamcrest.number.BigDecimalCloseTo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import exercises.model.ScheduleItem;

@RunWith(MockitoJUnitRunner.class)
public class OutputAdapterTest {

    OutputAdapter adapter;

    @Mock
    UserInterface ui;

    @Before
    public void setUp() throws Exception {
        adapter = new OutputAdapter(ui);
    }

    @Test
    public void testHeaderOutput() {
        List<ScheduleItem> items = Collections.emptyList();
        adapter.output(items);
        verify(ui, times(1)).printf("%1$-20s%2$-20s%3$-20s%4$s,%5$s,%6$s\n",
                                    "PaymentNumber",
                                    "PaymentAmount",
                                    "PaymentInterest",
                                    "CurrentBalance",
                                    "TotalPayments",
                                    "TotalInterestPaid");
        verifyNoMoreInteractions(ui);
    }

    @Test
    public void testScheduleOutput() {
        List<ScheduleItem> items = new ArrayList<ScheduleItem>();
        ScheduleItem scheduleItem1 = new ScheduleItem(1,
                                                      scale(123.45),
                                                      scale(12.34),
                                                      scale(987.65),
                                                      scale(789.01),
                                                      scale(234.56));
        items.add(scheduleItem1);
        adapter.output(items);
        verify(ui, times(1)).printf("%1$-20s%2$-20s%3$-20s%4$s,%5$s,%6$s\n",
                                    "PaymentNumber",
                                    "PaymentAmount",
                                    "PaymentInterest",
                                    "CurrentBalance",
                                    "TotalPayments",
                                    "TotalInterestPaid");

        verify(ui, times(1)).printf(Matchers.eq("%1$-20d%2$-20.2f%3$-20.2f%4$.2f,%5$.2f,%6$.2f\n"),
                                    Matchers.eq(1),
                                    Matchers.argThat(BigDecimalCloseTo.closeTo(scale(123.45), ERROR)),
                                    Matchers.argThat(BigDecimalCloseTo.closeTo(scale(12.34), ERROR)),
                                    Matchers.argThat(BigDecimalCloseTo.closeTo(scale(987.65), ERROR)),
                                    Matchers.argThat(BigDecimalCloseTo.closeTo(scale(789.01), ERROR)),
                                    Matchers.argThat(BigDecimalCloseTo.closeTo(scale(234.56), ERROR)));

        verifyNoMoreInteractions(ui);
    }

    private static final BigDecimal ERROR = new BigDecimal(0.0009);

    private static MathContext CONTEXT = MathContext.DECIMAL128;

    private static BigDecimal scale(double number) {
        return new BigDecimal(number, CONTEXT);
    }
}
