package exercises.view;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class InputValidatorTest {
	
	@Test
	public void testIsInRange_Double() {
		Double[] range = new Double[] {1d, 100d};
		assertTrue(InputValidator.isInRange(range, 50d));
		assertFalse(InputValidator.isInRange(range, 0d));
		assertTrue(InputValidator.isInRange(range, 1d));
		assertTrue(InputValidator.isInRange(range, 100d));
		assertFalse(InputValidator.isInRange(range, 101d));
	
	}

	@Test
	public void testIsInRange_Integer() {
		Integer[] range = new Integer[] {1, 100};
		assertTrue(InputValidator.isInRange(range, 50));
		assertFalse(InputValidator.isInRange(range, 0));
		assertTrue(InputValidator.isInRange(range, 1));
		assertTrue(InputValidator.isInRange(range, 100));
		assertFalse(InputValidator.isInRange(range, 101));
	
	}

}
