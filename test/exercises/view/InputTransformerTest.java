package exercises.view;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class InputTransformerTest {

    @Test
    public void testDoubleTransformer() {
        Double transformed = InputTransformer.DOUBLE_TRANSFORMER.transform("123.456");
        Double expected = Double.parseDouble("123.456");
        assertEquals(expected, transformed);
    }

    @Test
    public void testIntegerTransformer() {
        Integer transformed = InputTransformer.INTEGER_TRANSFORMER.transform("8383123");
        Integer expected = Integer.parseInt("8383123");
        assertEquals(expected, transformed);
    }
}
