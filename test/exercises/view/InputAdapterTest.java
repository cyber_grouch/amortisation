package exercises.view;

import static exercises.view.InputAdapter.getParser;
import static exercises.view.InputTransformer.DOUBLE_TRANSFORMER;
import static exercises.view.InputTransformer.INTEGER_TRANSFORMER;
import static exercises.view.InputValidator.APR_RANGE;
import static exercises.view.InputValidator.BORROW_AMOUNT_RANGE;
import static exercises.view.InputValidator.TERM_RANGE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InputAdapterTest {
	
	@Mock private UserInterface ui;
	
	InputAdapter adapter;

    @Before
    public void setUp() throws Exception {
        adapter = new InputAdapter(ui);
    }
    
	@Test
	public void testDoubleInput() throws IOException {
		String prompt = "Please enter the amount ";
        when(ui.readLine(prompt)).thenReturn("240000");
		adapter.prompt(prompt,
		               getParser(BORROW_AMOUNT_RANGE,
		                         DOUBLE_TRANSFORMER));
		verify(ui, times(1)).readLine(prompt);
        verifyNoMoreInteractions(ui);

	}
	
	@Test
	public void testAmountParser() throws IOException {
        String prompt = "Please enter the amount ";
        when(ui.readLine(prompt)).thenReturn("240000");
        UserInput<Double> input = adapter.prompt(prompt,
                                                 getParser(BORROW_AMOUNT_RANGE,
                                                           DOUBLE_TRANSFORMER));
        assertTrue(input.isValid());
        assertTrue(input.getParseValue().isPresent());
        assertFalse(input.getException().isPresent());
        assertFalse(input.getError().isPresent());        
        assertEquals(input.getParseValue().get(), (Double) 240000d);
	}

	@Test
	public void testIntegerInput() throws IOException {
		String prompt = "Please enter the amount ";
		when(ui.readLine(prompt)).thenReturn("240000");
		
		UserInput<Integer> input = adapter.prompt(prompt,
		                                          getParser(TERM_RANGE,
		                                                    INTEGER_TRANSFORMER));
		assertTrue(input.isValid());  
		assertTrue(input.getParseValue().isPresent());
        assertFalse(input.getException().isPresent());
        assertFalse(input.getError().isPresent());        
		assertEquals(input.getParseValue().get(), (Integer) 240000);
	}
	
    @Test
    public void testErroneousInput() throws IOException {
        String prompt = "Please enter the amount ";
        when(ui.readLine(prompt)).thenReturn("abcde");
        
        UserInput<Integer> input = adapter.prompt(prompt,
                                                  getParser(TERM_RANGE,
                                                            INTEGER_TRANSFORMER));
        assertFalse(input.isValid());
        assertFalse(input.getParseValue().isPresent());
        assertTrue(input.getException().isPresent());
        assertFalse(input.getError().isPresent());
    }

    @Test
    public void testInvalidInput() throws IOException {
        String prompt = "Please enter the amount ";
        when(ui.readLine(prompt)).thenReturn("200");
        
        UserInput<Double> input = adapter.prompt(prompt,
                                                 getParser(APR_RANGE,
                                                           DOUBLE_TRANSFORMER));
        assertFalse(input.isValid());
        assertFalse(input.getParseValue().isPresent());
        assertFalse(input.getException().isPresent());
        assertTrue(input.getError().isPresent());
    }
}
