package exercises;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import exercises.model.ScheduleParametersTest;
import exercises.schedule.CalculatorTest;
import exercises.schedule.CalculatorUtilsTest;
import exercises.schedule.ScheduleGeneratorTest;
import exercises.view.InputAdapterTest;
import exercises.view.InputTransformerTest;
import exercises.view.InputValidatorTest;
import exercises.view.OutputAdapterTest;

@RunWith(Suite.class)
@SuiteClasses({ ScheduleGeneratorTest.class,
                CalculatorUtilsTest.class,
                CalculatorTest.class,
                ScheduleParametersTest.class,
                InputTransformerTest.class,
                InputValidatorTest.class,
                OutputAdapterTest.class,
                InputAdapterTest.class })
public class AllTests {

}
