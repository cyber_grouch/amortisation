package exercises.model;

public class ScheduleParameters {
    private final double amount;
    private final double interestRate;
    private final int years;

    public ScheduleParameters(double amount, double interestRate, int years) {
        super();
        this.amount = amount;
        this.interestRate = interestRate;
        this.years = years;
    }
    
    public double getAmount() {
        return amount;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public int getYears() {
        return years;
    }
    
}
