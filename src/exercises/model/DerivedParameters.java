package exercises.model;

import java.math.BigDecimal;

public class DerivedParameters {

    private final ScheduleParameters scheduleParameters;
    private final BigDecimal amountBorrowed;
    private final BigDecimal monthlyInterest;
    private final BigDecimal monthlyPaymentAmount;
    private final int months;
    private final boolean isInvalid;

    public DerivedParameters(ScheduleParameters scheduleParameters,
                             BigDecimal amountBorrowed,
                             BigDecimal monthlyInterest,
                             BigDecimal monthlyPaymentAmount,
                             int months,
                             boolean isInvalid) {
        super();
        this.scheduleParameters = scheduleParameters;
        this.amountBorrowed = amountBorrowed;
        this.monthlyInterest = monthlyInterest;
        this.monthlyPaymentAmount = monthlyPaymentAmount;
        this.months = months;
        this.isInvalid = isInvalid;
    }
    
    public ScheduleParameters getScheduleParameters() {
        return scheduleParameters;
    }

    public BigDecimal getAmountBorrowed() {
        return amountBorrowed;
    }

    public BigDecimal getMonthlyInterest() {
        return monthlyInterest;
    }

    public BigDecimal getMonthlyPaymentAmount() {
        return monthlyPaymentAmount;
    }

    public int getMonths() {
        return months;
    }

    public boolean isInvalid() {
        return isInvalid;
    }

}
