package exercises.model;

import java.math.BigDecimal;

public class ScheduleItem {
    private final int paymentNumber;
    private final BigDecimal curMonthlyPaymentAmount;
    private final BigDecimal curMonthlyInterest;
    private final BigDecimal curBalance;
    private final BigDecimal totalPayments;
    private final BigDecimal totalInterestPaid;
    
    public ScheduleItem(int paymentNumber,
                        BigDecimal curMonthlyPaymentAmount,
                        BigDecimal curMonthlyInterest, 
                        BigDecimal curBalance,
                        BigDecimal totalPayments, 
                        BigDecimal totalInterestPaid) {
        super();
        this.paymentNumber = paymentNumber;
        this.curMonthlyPaymentAmount = curMonthlyPaymentAmount;
        this.curMonthlyInterest = curMonthlyInterest;
        this.curBalance = curBalance;
        this.totalPayments = totalPayments;
        this.totalInterestPaid = totalInterestPaid;
    }

    public int getPaymentNumber() {
        return paymentNumber;
    }

    public BigDecimal getCurMonthlyPaymentAmount() {
        return curMonthlyPaymentAmount;
    }

    public BigDecimal getCurMonthlyInterest() {
        return curMonthlyInterest;
    }

    public BigDecimal getCurBalance() {
        return curBalance;
    }

    public BigDecimal getTotalPayments() {
        return totalPayments;
    }

    public BigDecimal getTotalInterestPaid() {
        return totalInterestPaid;
    }

}
