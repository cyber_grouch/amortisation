package exercises;

import java.util.List;
import java.util.Optional;

import exercises.model.ScheduleItem;
import exercises.model.ScheduleParameters;
import exercises.schedule.ScheduleGenerator;
import exercises.view.UserInterface;

public class AmortizationSchedule {

    public static void main(String[] args) {
        UserInterface ui = UserInterface.getInstance();
        ScheduleParameters scheduleParameters = ui.input();

        ScheduleGenerator generator = new ScheduleGenerator();
        Optional<List<ScheduleItem>> generatedSchedule = generator.generateSchedule(scheduleParameters);
        if (!generatedSchedule.isPresent()) {
            ui.print("Unable to process the values entered. Terminating program.\n");
            System.exit(-1);  
        }
        
        ui.output(generatedSchedule.get());
    }


}
