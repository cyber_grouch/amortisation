package exercises.view;

import java.util.Optional;

public class UserInput<T> {

    public static <T> UserInput<T> validInput(String input, T parseValue) {
        return new UserInput<T>(true, input, Optional.empty(), Optional.empty(), Optional.of(parseValue));
    }

    public static <T> UserInput<T> invalidInput(String input, String error) {
        return new UserInput<T>(false, input, Optional.of(error), Optional.empty(), Optional.empty());
    }

    public static <T> UserInput<T> errorInput(String input, Exception error) {
        return new UserInput<T>(false, input, Optional.empty(), Optional.of(error), Optional.empty());
    }

    private final String input;
    private final boolean valid;
    private final Optional<String> error;
    private final Optional<Exception> exception;
    private final Optional<T> parseValue;

    private UserInput(boolean valid, String input, Optional<String> error, Optional<Exception> exception, Optional<T> parseValue) {
        this.input = input;
        this.valid = valid;
        this.error = error;
        this.exception = exception;
        this.parseValue = parseValue;
    }

    public String getInput() {
        return input;
    }

    public boolean isValid() {
        return valid;
    }

    public Optional<String> getError() {
        return error;
    }

    public Optional<Exception> getException() {
        return exception;
    }

    public Optional<T> getParseValue() {
        return parseValue;
    }

}
