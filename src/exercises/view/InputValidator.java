package exercises.view;

public class InputValidator {

    public static final String ERROR_TEMPLATE = "Please enter a positive value between %s and %s. ";

    public static final Double[] BORROW_AMOUNT_RANGE = new Double[] { 0.01d, 1000000000000d };

    public static final Double[] APR_RANGE = new Double[] { 0.000001d, 100d };

    public static final Integer[] TERM_RANGE = new Integer[] { 1, 1000000 };

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static boolean isInRange(Comparable[] range, Comparable input) {
        return (range[0].compareTo(input) <= 0) && (input.compareTo(range[1]) <= 0);
    }

}
