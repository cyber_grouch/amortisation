package exercises.view;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Consumer;

import exercises.model.ScheduleItem;

public class OutputAdapter {
    
    private final UserInterface ui;

    public OutputAdapter(UserInterface ui) {
        this.ui = ui;
    }

    public void output(List<ScheduleItem> generatedSchedule) {
        String formatString = "%1$-20s%2$-20s%3$-20s%4$s,%5$s,%6$s\n";

        ui.printf(formatString,
                "PaymentNumber", "PaymentAmount", "PaymentInterest",
                "CurrentBalance", "TotalPayments", "TotalInterestPaid");
        
        String dollarFormatString = "%1$-20d%2$-20.2f%3$-20.2f%4$.2f,%5$.2f,%6$.2f\n";

        generatedSchedule.stream().forEach(new Consumer<ScheduleItem>() {
            @Override
            public void accept(ScheduleItem t) {
                // output is in dollars
                ui.printf(dollarFormatString,
                          t.getPaymentNumber(),
                          scale(t.getCurMonthlyPaymentAmount()),
                          scale(t.getCurMonthlyInterest()),
                          scale(t.getCurBalance()),
                          scale(t.getTotalPayments()),
                          scale(t.getTotalInterestPaid()));
            }
        });
    }
    
    public BigDecimal scale(BigDecimal value) {
        return value.setScale(6, BigDecimal.ROUND_HALF_UP);
    }
}
    
