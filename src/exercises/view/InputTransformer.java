package exercises.view;

@SuppressWarnings("rawtypes")
public interface InputTransformer<T extends Comparable> {

    T transform(String string);

    public static InputTransformer<Double> DOUBLE_TRANSFORMER = new InputTransformer<Double>() {
        @Override
        public Double transform(String string) {
            return Double.parseDouble(string);
        }
    };

    public static InputTransformer<Integer> INTEGER_TRANSFORMER = new InputTransformer<Integer>() {
        @Override
        public Integer transform(String string) {
            return Integer.parseInt(string);
        }
    };
}
