package exercises.view;

import static exercises.view.InputTransformer.DOUBLE_TRANSFORMER;
import static exercises.view.InputTransformer.INTEGER_TRANSFORMER;
import static exercises.view.InputValidator.APR_RANGE;
import static exercises.view.InputValidator.BORROW_AMOUNT_RANGE;
import static exercises.view.InputValidator.TERM_RANGE;

import java.io.IOException;
import java.util.function.Function;

import exercises.model.ScheduleParameters;

public class InputAdapter {
    
    private final UserInterface ui;

    public InputAdapter(UserInterface ui) {
        this.ui = ui;
    }

    public ScheduleParameters input() {
        String[] userPrompts = { 
            "Please enter the amount you would like to borrow: ",
            "Please enter the annual percentage rate used to repay the loan: ",
            "Please enter the term, in years, over which the loan is repaid: "
        };

        double amount = 0;
        double apr = 0;
        int years = 0;

        for (int i = 0; i < userPrompts.length; i++) {
            String userPrompt = userPrompts[i];
            
            switch (i) {
            case 0:
                amount = getInput(userPrompt, BORROW_AMOUNT_RANGE, DOUBLE_TRANSFORMER);
                break;
            case 1:
                apr = getInput(userPrompt, APR_RANGE, DOUBLE_TRANSFORMER);
                break;
            case 2:
                years = getInput(userPrompt, TERM_RANGE, INTEGER_TRANSFORMER);
                break;
            }
        }
                            
        return new ScheduleParameters(amount, apr, years);
    }
    
    @SuppressWarnings("rawtypes")
    public static <T extends Comparable>
            Function<String, UserInput<T>> getParser(Object[] range, InputTransformer<T> parser) {
        return new Function<String, UserInput<T>>() {
            @SuppressWarnings("unchecked")
            @Override
            public UserInput<T> apply(String line) {
                String error = String.format(InputValidator.ERROR_TEMPLATE, range[0], range[1]);
                T amount = null;
                try {
                    amount = parser.transform(line);
                } catch (NumberFormatException e) {
                    return UserInput.errorInput(line, new IllegalArgumentException(error));
                }
                if (!InputValidator.isInRange((T[]) range, amount)) {
                    return UserInput.invalidInput(line, error);
                }
                return UserInput.validInput(line, amount);
            }
        };
    }
    
    public <T> UserInput<T> prompt(String prompt, Function<String, UserInput<T>> transformer) {
        String userInput = "";
        try {
            userInput = ui.readLine(prompt);
        } catch (IOException e) {
            return UserInput.errorInput("<error>", e);
        }
        return transformer.apply(userInput);
    }

    @SuppressWarnings("rawtypes")
    public <T extends Comparable> T getInput(String prompt,
                                             T[] range,
                                             InputTransformer<T> transformer) {
        boolean isValidInput = false;
        UserInput<T> input = null;
        while (!isValidInput) {
            input = prompt(prompt, getParser(range, transformer));
            if (!input.isValid()) {
                if (input.getException().isPresent()) {
                    ui.print("An IOException was encountered. Terminating program.\n");
                    System.exit(-1);
                }
                ui.print(input.getError().get());
            }
            isValidInput = input.getParseValue().isPresent();
        }
        return input.getParseValue().get();
    }

}
