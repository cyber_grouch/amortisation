package exercises.view;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.IllegalFormatException;
import java.util.List;

import exercises.model.ScheduleItem;
import exercises.model.ScheduleParameters;

public class UserInterface {

    private final Console console;
    private final OutputAdapter outputAdapter;
    private final InputAdapter inputAdapter;
    
    private static class Holder {
        public static UserInterface INSTANCE = new UserInterface();
     }

    public static final UserInterface getInstance() {
        return Holder.INSTANCE;
    }

    private UserInterface() {
        super();
        this.console = System.console();
        this.outputAdapter = new OutputAdapter(this);
        this.inputAdapter = new InputAdapter(this);
    }

    public void printf(String formatString, Object... args) {
        try {
            if (console != null) {
                console.printf(formatString, args);
            } else {
                System.out.print(String.format(formatString, args));
            }
        } catch (IllegalFormatException e) {
            System.err.print("Error printing...\n");
        }
    }

    public void print(String s) {
        printf("%s", s);
    }

    public String readLine(String userPrompt) throws IOException {
        String line = "";

        if (console != null) {
            line = console.readLine(userPrompt);
        } else {
            // print("console is null\n");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

            print(userPrompt);
            line = bufferedReader.readLine();
        }
        line.trim();
        return line;
    }
    
    public void output(List<ScheduleItem> items) {
        this.outputAdapter.output(items);
    }

    public ScheduleParameters input() {
        return this.inputAdapter.input();
    }
    
}
