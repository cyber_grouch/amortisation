package exercises.schedule;

import java.math.BigDecimal;
import java.math.MathContext;

public class CalculatorUtils {
    
    private static MathContext CONTEXT = MathContext.DECIMAL128;
    
    public static BigDecimal scale(double number) {
        return new BigDecimal(number, CONTEXT);
    }
    
    public static BigDecimal inverse(BigDecimal number) {
        return power(number, -1);
    }
    
    public static BigDecimal power(BigDecimal number, int power) {
        return number.pow(power, CONTEXT);
    }

    public static BigDecimal divide(BigDecimal dividend, BigDecimal divisor) {
        return dividend.divide(divisor, CONTEXT);
    }
}
