package exercises.schedule;

import static exercises.schedule.CalculatorUtils.divide;
import static exercises.schedule.CalculatorUtils.inverse;
import static exercises.schedule.CalculatorUtils.power;
import static exercises.schedule.CalculatorUtils.scale;

import java.math.BigDecimal;
import java.util.Optional;

import exercises.model.DerivedParameters;
import exercises.model.ScheduleItem;
import exercises.model.ScheduleParameters;

public class Calculator {

    private final BigDecimal amountBorrowed;
    private final BigDecimal monthlyInterest;
    private final BigDecimal monthlyPaymentAmount;
    private final int months;
    private final boolean isInvalid;
    private final DerivedParameters derivedParameters;
    
    private static final BigDecimal MONTHS_PER_YEAR = scale(12);

    public Calculator(ScheduleParameters scheduleParameters) {
        this.months = scheduleParameters.getYears() * 12;
        
        this.amountBorrowed = scale(scheduleParameters.getAmount());

        // M = P * (J / (1 - (Math.pow(1/(1 + J), N))));
        //
        // Where:
        // P = Principal
        // I = Interest
        // J = Monthly Interest in decimal form: I / (12 * 100)
        // N = Number of months of loan
        // M = Monthly Payment Amount
        //

        // calculate J
        this.monthlyInterest = divide(scale(scheduleParameters.getInterestRate()), MONTHS_PER_YEAR.scaleByPowerOfTen(2));

        // this is 1 / (1 + J)
        BigDecimal tmp = inverse(monthlyInterest.add(BigDecimal.ONE));

        // this is Math.pow(1/(1 + J), N)
        tmp = power(tmp, months);

        // this is 1 / (1 - (Math.pow(1/(1 + J), N))))
        tmp = inverse(tmp.negate().add(BigDecimal.ONE));

        // M = P * (J / (1 - (Math.pow(1/(1 + J), N))));
        this.monthlyPaymentAmount = amountBorrowed.multiply(monthlyInterest).multiply(tmp);

        // the following shouldn't not evaluate to true the available valid
        // ranges
        // for borrow amount, apr, and term; however, without range validation,
        // monthlyPaymentAmount as calculated by calculateMonthlyPayment()
        // may yield incorrect values with extreme input values
        this.isInvalid = monthlyPaymentAmount.compareTo(amountBorrowed) > 0;
        
        this.derivedParameters = new DerivedParameters(scheduleParameters,
                                                       amountBorrowed,
                                                       monthlyInterest,
                                                       monthlyPaymentAmount,
                                                       months,
                                                       isInvalid);
    }
    
    public DerivedParameters getDerivedParameters() {
        return derivedParameters;
    }
    
    public ScheduleItem getFirstScheduleItem() {
        return new ScheduleItem(0,
                                BigDecimal.ZERO,
                                BigDecimal.ZERO,
                                amountBorrowed,
                                BigDecimal.ZERO,
                                BigDecimal.ZERO);
    }

    public Optional<ScheduleItem> getNextScheduleItem(ScheduleItem scheduleItem) {
        if (BigDecimal.ZERO.compareTo(scheduleItem.getCurBalance().setScale(2, BigDecimal.ROUND_DOWN)) >= 0) {
            return Optional.empty();
        }

        BigDecimal balance = scheduleItem.getCurBalance();
        
        // Calculate H = P x J, this is your current monthly interest
        BigDecimal curMonthlyInterest = balance.multiply(derivedParameters.getMonthlyInterest());

        // the amount required to payoff the loan
        BigDecimal curPayoffAmount = balance.add(curMonthlyInterest);
        
        // the amount to payoff the remaining balance may be less than the calculated monthlyPaymentAmount
        BigDecimal curMonthlyPaymentAmount = derivedParameters.getMonthlyPaymentAmount().min(curPayoffAmount);
        
        // it's possible that the calculated monthlyPaymentAmount is 0,
        // or the monthly payment only covers the interest payment - i.e. no principal
        // so the last payment needs to payoff the loan
        if ((curMonthlyPaymentAmount.compareTo(BigDecimal.ZERO) == 0)
                || (curMonthlyPaymentAmount.compareTo(curMonthlyInterest) == 0)) {
            curMonthlyPaymentAmount = curPayoffAmount;
        }
        
        // Calculate C = M - H, this is your monthly payment minus your monthly interest,
        // so it is the amount of principal you pay for that month
        BigDecimal curMonthlyPrincipalPaid = curMonthlyPaymentAmount.subtract(curMonthlyInterest);
        
        // Calculate Q = P - C, this is the new balance of your principal of your loan.
        BigDecimal curBalance = balance.subtract(curMonthlyPrincipalPaid);
        
        BigDecimal totalPayments = scheduleItem.getTotalPayments().add(curMonthlyPaymentAmount);
        BigDecimal totalInterestPaid = scheduleItem.getTotalInterestPaid().add(curMonthlyInterest);
        
        return Optional.of(new ScheduleItem(scheduleItem.getPaymentNumber() + 1,
                                            curMonthlyPaymentAmount,
                                            curMonthlyInterest,
                                            curBalance,
                                            totalPayments,
                                            totalInterestPaid));
    }

}
